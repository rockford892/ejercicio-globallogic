package com.ejercicio.globallogic.helper;

public class MessageHelper {
    public static final String MESSAGE = "mensaje";
    public static final String MESSAGE_NOT_NULL = "Dato no puede ser nulo";
    public static final String MESSAGE_ERROR_CHECK_PASS = "error en password";
    public static final String MESSAGE_EMPTY_COLLECTION = "minimo de caracteres 1";
    public static final String MESSAGE_EMAIL_EXISTS = "El correo ya registrado";
    public static final String MESSAGE_ERROR_REGEXP = "mal formado";

    private MessageHelper() {
    }
}
