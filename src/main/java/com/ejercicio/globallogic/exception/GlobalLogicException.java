package com.ejercicio.globallogic.exception;

public class GlobalLogicException extends Exception {
    public GlobalLogicException(String message) {
        super(message);
    }
}
